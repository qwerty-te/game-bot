import json

MAX_PLAYERS = 2
MIN_PLAYERS = 2


def handle(message, group, bot):
    """

    :type message: telebot.types.Message
    :type user: tables.Group
    :type bot: telebot.TeleBot
    :return:
    """
    state = group.get_state()
    if state['status'] == 'final':
        bot.send_message(message.chat.id, f"game is ended")
        return

    if message.from_user.id != state['turn']:
        bot.send_message(message.chat.id, f"not your turn")
        return

    if state['sticks'] == 0:
        bot.send_message(message.chat.id, f"game is ended")
        state['status'] = 'final'
        group.set_state(state)
        return

    if state['sticks'] == 1:
        bot.send_message(message.chat.id, f"{message.from_user.username} you lose")
        state['turn'] = ''
        state['status'] = 'final'
        state['sticks'] = 0
        group.set_state(state)
        return

    move = message.text.split('@', 1)[0].strip('/')

    if move not in {'1', '2', '3'}:
        bot.send_message(message.chat.id, f"move should be /1 or /2 or /3")
        return
    if state['sticks'] < int(move):
        bot.send_message(message.chat.id, f"{message.from_user.username} you can't do this")
        return
    state['sticks'] -= int(move)
    bot.send_message(message.chat.id, f"sticks left {state['sticks']}\n /1 /2 /3 ")
    users = group.get_users()
    index = users.index(state['turn'])
    if index == len(users) - 1:
        state['turn'] = users[0]
    else:
        state['turn'] = users[index + 1]

    group.set_state(state)
    if state['sticks'] == 0:
        bot.send_message(message.chat.id, f"{message.from_user.username} you lose")
    elif state['sticks'] == 1:
        bot.send_message(message.chat.id, f"{message.from_user.username} you win")


def start_message(message, group, bot):
    """

    :type message: telebot.types.Message
    :type group: tables.Group
    :type bot: telebot.TeleBot
    :return:
    """
    bot.send_message(message.chat.id, "start")
    # user.state =

    bot.send_message(message.chat.id, "/1 /2 /3 ")
    group.set_state({'status': 'start', 'sticks': 20, 'turn': group.get_users()[0]})
    bot.send_message(message.chat.id, f"sticks left 20")
