
from telebot import TeleBot
# from telebot import apihelper
from telebot import types
import sticks

from tables import User, Group


BOT_TOKEN = "521789682:AAE4Z6wsBXOtsUpQu4RhYNP-MyMRrZUOQ0U"

bot = TeleBot(BOT_TOKEN)
User.create_table()
Group.create_table()

GAMES = {'sticks': sticks}
GROUP = 'group'


@bot.message_handler(commands=['start'])
def start(message):
    bot.reply_to(message, "Howdy, how are you doing?")
    # bot.reply_to(message, "/help")
    if message.chat.type == GROUP:
        group, created = Group.get_or_create(chat_id=message.chat.id)
        group.state = ''
        group.last_game = ''
        group.users = ''
        group.save()
        # bot.send_message(message.chat.id, '/accept')
        for i in GAMES:
            bot.send_message(message.chat.id, f'/{i}')
    # else:
    #     user, created = User.get_or_create(telegram_id=message.from_user.id)
    #     user.state = ''
    #     user.last_game = ''
    #     user.save()
    #     bot.send_message(message.chat.id, user.id)
    #     bot.send_message(message.chat.id, [f'/{key}' for key in GAMES])


@bot.message_handler(commands=GAMES.keys())
def play(message):
    group = Group.get(chat_id=message.chat.id)
    keyboard = types.InlineKeyboardMarkup()
    key_yes = types.InlineKeyboardButton(text='Буду 0', callback_data='accept')
    keyboard.add(key_yes)
    key_no = types.InlineKeyboardButton(text='Без меня', callback_data='decline')
    keyboard.add(key_no)
    key_start = types.InlineKeyboardButton(text='Пора!', callback_data='start')
    keyboard.add(key_start)
    bot.send_message(message.chat.id, 'Будешь?', reply_markup=keyboard)
    group.last_game = message.text.split('@', 1)[0].strip('/')
    group.save()


def true(call):
    print(call)
    return True


@bot.callback_query_handler(func=true)
def callback_worker(call):
    group = Group.get(chat_id=call.message.chat.id)
    users = group.get_users()
    if call.data == "accept":
        if call.from_user.id not in users:
            group.set_users(users + [call.from_user.id])
        bot.send_message(call.from_user.id, 'Запомню : )')
    elif call.data == "decline":
        if call.from_user.id in users:
            users.remove(call.from_user.id)
            group.set_users(users)
        bot.send_message(call.from_user.id, 'Вычёркиваю : )')
    elif call.data == "start":
        game = GAMES[group.last_game]
        bot.send_message(call.message.chat.id, 'Поехали')
        game.start_message(call.message, group, bot)
    update_keyboard(call, group.players_count())


def update_keyboard(call, players_count):
    keyboard = types.InlineKeyboardMarkup()
    key_yes = types.InlineKeyboardButton(text='Буду {}'.format(players_count), callback_data='accept')
    keyboard.add(key_yes)
    key_no = types.InlineKeyboardButton(text='Без меня', callback_data='decline')
    keyboard.add(key_no)
    key_start = types.InlineKeyboardButton(text='Пора!', callback_data='start')
    keyboard.add(key_start)
    bot.edit_message_reply_markup(
        chat_id=call.message.chat.id, message_id=call.message.message_id, reply_markup=keyboard)

#
# @bot.message_handler(commands=['accept'])
# def accept(message):
#     group = Group.get(chat_id=message.chat.id)
#
#     if not group.state:
#         users = group.get_users()
#         if message.from_user.id not in users:
#             group.set_users(users+[message.from_user.id])
#             bot.send_message(message.chat.id, 'added')
#         else:
#             bot.send_message(message.chat.id, 'already added')
#     else:
#         bot.send_message(message.chat.id, 'you are late')
#
#
# @bot.message_handler(commands=['accept'])
# def accept(message):
#     group = Group.get(chat_id=message.chat.id)
#     if message.from_user.id in group.get_users():
#         bot.send_message(message.from_user.id, 'Your previous game was' + group.last_game)
#         bot.send_message(message.from_user.id, 'You state' + group.state)
#         group.last_game = message.text[1:].split('@')[0]
#         group.state = ''
#         group.save()
#         game = GAMES[group.last_game]
#         game.start_message(message, group, bot)
#     else:
#         bot.send_message(message.chat.id, 'you are not in party')


@bot.message_handler(content_types=['text'])
def handle(message):
    """

    :type message: telebot.types.Message
    :return:
    """
    group = Group.get(chat_id=message.chat.id)
    game = GAMES[group.last_game]
    game.handle(message, group, bot)
    # if user.last_game:
    #     game = GAMES[user.last_game]
    #     game.handle(message, user, bot)
    # else:
    #     bot.reply_to(message, "you are not in game")


bot.polling(none_stop=True)
