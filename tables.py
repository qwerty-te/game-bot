import json
from peewee import *

db = SqliteDatabase('puzzle.sqlite')


class User(Model):
    telegram_id = BigIntegerField(null=False, index=True, unique=True)
    last_game = CharField(default='')
    state = TextField(default='')

    def get_state(self):
        return self.state

    class Meta:
        database = db


class Group(Model):
    chat_id = BigIntegerField(null=False, index=True, unique=True)
    last_game = CharField(default='')
    state = TextField(default='')
    users = TextField(default='')

    def get_users(self):
        return [int(i) for i in self.users.split(',')] if self.users else []

    def set_users(self, users):
        """

        :type users: list
        :return:
        """
        self.users = str(','.join(map(str, users)))
        self.save()

    def players_count(self):
        if not self.users:
            return 0
        else:
            return self.users.count(',') + 1

    def get_state(self):
        return json.loads(self.state)

    def set_state(self, state):
        self.state = json.dumps(state)
        self.save()

    class Meta:
        database = db
